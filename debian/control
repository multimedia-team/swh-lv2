Source: swh-lv2
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper-compat (= 13),
 libfftw3-dev,
 libgsm1-dev,
 lv2-dev,
 xsltproc
Homepage: https://github.com/swh/lv2
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/multimedia-team/swh-lv2.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/swh-lv2

Package: swh-lv2
Provides:
 lv2-plugin
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Steve Harris's SWH plugins ported to LV2
 This package provides Steve Harris's SWH plugins ported to the
 LV2 specification.
 .
 There is a large number of effects, such as filters, harmonic
 generators, pitch shifters and much more, for LV2 compatible
 hosts (Qtractor, Ardour, lv2rack). Here is a list of provided
 effects:
 .
 inv, flanger, diode, comb, notch_iir, sc2, debug, amp, zm1,
 matrix_spatialiser, bandpass_iir, lookahead_limiter_const,
 fast_lookahead_limiter, am_pitchshift, revdelay, valve_rect,
 delayorama, harmonic_gen, sifter, decimator, foverdrive,
 matrix_st_ms, step_muxer, tape_delay, pointer_cast, xfade,
 dj_flanger, single_para, dc_remove, satan_maximiser, phasers,
 foldover, matrix_ms_st, freq_tracker, bode_shifter, alias,
 dyson_compress, dj_eq, comb_splitter, hermes_filter, valve,
 bode_shifter_cv, multivoice_chorus, plate, latency, fad_delay,
 split, allpass, const, hilbert, fm_osc, sin_cos, crossover_dist,
 triple_para, hard_limiter, imp, chebstortion, vynil, svf,
 rate_shifter, surround_encoder, se4, sc1, lookahead_limiter,
 divider, impulse, lowpass_iir, karaoke, sc4, shaper, butterworth,
 gong_beater, gsm, sinus_wavewrapper, declip, gverb, transient,
 pitch_scale, ls_filter, ringmod, sc3, gate, delay, giant_flange,
 lcr_delay, mod_delay, smooth_decimate, wave_terrain, highpass_iir,
 analogue_osc, retro_flange, mbeq, simple_comb, gong, decay,
 bandpass_a_iir
